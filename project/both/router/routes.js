/*****************************************************************************/
/* Client and Server Routes */
/*****************************************************************************/
Router.configure({
  layoutTemplate: 'MasterLayout',
  loadingTemplate: 'Loading',
  notFoundTemplate: 'NotFound'
});

Router.route('/', {name: 'home'});
Router.route('/scenarios', {name: 'scenarios'});
Router.route('/npcharacters', {name: 'Npcharacter'});
Router.route('/npcharacterrequest', {name: 'NPCharacterRequest'});
Router.route('/npcharacterresponse', {name: 'NpcharacterResponse'});
Router.route('/playerrequest', {name: 'PlayerRequest'});
Router.route('/playerresponse', {name: 'PlayerResponse'});